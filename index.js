var CSGO = require('./csgo'),
	async = require('async'),
	express = require('express'),
	nunjucks = require('nunjucks');

var CSGORanks = function CSGORanks(args) {	
	csgo = new CSGO.CSGOClient({
		accountName: args.accountName,
		password: args.password,
		sentryFileName: args.sentryFileName,
		webapiKey: args.webapiKey
	});

	app = express();
	app.enable('trust proxy');
	// app.set('models', require('./models'));
	// csgo.setModels(app.get('models'));
	app.use(require('morgan')('combined'));
	env = nunjucks.configure('templates', {
		autoescape: true,
		express: app
	});
	if (args.analyticsCode)
		env.addGlobal('analytics', args.analyticsCode); 

	app.get('/', function(req, res) {
		res.render('index.html');
	});

	app.get('/profiles', function(req, res) {
		if (!('steamid' in req.query)) {
			return res.redirect('../');
		}
		if (req.query.status) {
			input = req.query.steamid.match(/STEAM_1:[01]:\d+/g) || [];
		} else {
			input = req.query.steamid.replace(/\n/g, ";").split(";");
		}
		async.map(input, csgo.toAccountIDAsync.bind(csgo), function(err, accountIds) {
			accountIds = accountIds.filter(function(id) {
				return (id > 0);
			});
			accountIds = accountIds.filter(function(el, pos) {
				return accountIds.indexOf(el) == pos;
			});
			csgo.getRanks(accountIds, req.query.ranks, function(profileData) {
				if (req.xhr || req.query.raw) {
					delete profileData.showRanks;
					res.send(profileData);
				} else {
					res.render('ranks.html', profileData);
				}
			});
		});
	});

	app.listen(args.httpPort);
};

module.exports.CSGORanks = CSGORanks;