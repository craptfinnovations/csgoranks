/*
	CS:GO MM rank retriever. Includes cooldown info.
*/

var EventEmitter = require('events').EventEmitter,
	fs = require('fs'),
	util = require('util'),
	bignum = require('bignum'),
	moment = require('moment'),
	Steam = require('steam'),
	webAPI = require('steam-webapi'),
	Schema = require('protobuf').Schema,
	winston = require('winston'),
	base_gcmessages = new Schema(fs.readFileSync(__dirname + "/generated/base_gcmessages.desc")),
	gcsdk_gcmessages = new Schema(fs.readFileSync(__dirname + "/generated/gcsdk_gcmessages.desc")),
	cstrike15_gcmessages = new Schema(fs.readFileSync(__dirname + "/generated/cstrike15_gcmessages.desc")),
	steammessages = new Schema(fs.readFileSync(__dirname + "/generated/steammessages.desc")),
	protoMask = 0x80000000,
	CSGO = exports;

winston.add(winston.transports.File, {filename: 'csgoranks.log'});

CSGO.EMsg = {
	ClientWelcome: 4004,
	ClientHello: 4006,
	ClientConnectionStatus: 4009,
	ClientRequestPlayersProfile: 9127,
	PlayersProfile: 9128,
};
CSGO.MMRanks = [
	"", "Silver I", "Silver II", "Silver III", "Silver IV", "Silver Elite",
	"Silver Elite Master", "Gold Nova I", "Gold Nova II", "Gold Nova III",
	"Gold Nova Master", "Master Guardian I", "Master Guardian II",
	"Master Guardian Elite", "Distinguished Master Guardian",
	"Legendary Eagle", "Legendary Eagle Master",
	"Supreme Master First Class", "The Global Elite",
];
CSGO.MMRanksAbbr = [
	"", "S1", "S2", "S3", "S4", "SE", "SEM", "GN1", "GN2", "GN3", "GNM",
	"MG1", "MG2", "MGE", "DMG", "LE", "LEM", "SMFC", "GE",
];
CSGO.PenaltyReasons = {
	4: "Failed to reconnect to the last match",
	5: "Abandoned the last match",
	10: "Convicted for cheating",

	1001: "Kicked from the last match",
	1003: "Minimal cooldown, abandoned the last match",
	1004: "Minimal cooldown, failed to reconnect to the last match",
	1005: "Failed to connect by match start",
	1006: "Killed a teammate at round start",
	1007: "Killed too many teammates",
	1008: "Did too much damage to teammates at round start",
	1009: "Did too much damage to teammates",
	1010: "Account currently untrusted",
	1011: "Kicked from too many recent matches",
	1012: "Kicked too many teammates in recent matches",
	1013: "Convicted for griefing",
};
CSGO.GCConnectionStatus = {
	GCConnectionStatus_HAVE_SESSION: 0,
	GCConnectionStatus_GC_GOING_DOWN: 1,
	GCConnectionStatus_NO_SESSION: 2,
	GCConnectionStatus_NO_SESSION_IN_LOGON_QUEUE: 3,
	GCConnectionStatus_NO_STEAM: 4
};

var CSGOClient = function CSGOClient(args) {
	this.steam = new Steam.SteamClient();
	webAPI.key = args.webapiKey;
	this._appid = 730;
	this._gcClientHelloIntervalId = null;
	this.ready = false;

	var self = this;
	webAPI.ready(function(err) {
		if (err) return winston.log('warn', err);
		self.webapi = new webAPI();
	});

	if (fs.existsSync('servers')) {
		Steam.servers = JSON.parse(fs.readFileSync('servers'));
	}

	var logonDetails = {
		accountName: args.accountName,
		password: args.password,
	};
	if (args.sentryFileName || false) {
		logonDetails.shaSentryfile = require('crypto').createHash('sha1').update(fs.readFileSync(args.sentryFileName)).digest();
	}
	this.steam.logOn(logonDetails);

	this.steam.on('loggedOn', function() {
		self.steam.setPersonaState(Steam.EPersonaState.Online);
		self.steam.gamesPlayed([self._appid]);
		self._gcClientHelloIntervalId = setInterval(self._sendHello, 2500);
	});

	this.steam.on('fromGC', function(appid, type, message, callback) {
		callback = callback || null;
		var kMsg = type & ~protoMask;

		if (kMsg in self._handlers) {
			if (callback)
				self._handlers[kMsg].call(self, message, callback);
			else
				self._handlers[kMsg].call(self, message);
		} else {
			winston.info('Unknown message: ' + kMsg);
		}
	});

	this._sendHello = function() {
		self.steam.toGC(self._appid, CSGO.EMsg.ClientHello | protoMask, gcsdk_gcmessages.CMsgClientHello.serialize({
			version: 0,
			socacheHaveVersions: []
		}));
	};
};
util.inherits(CSGOClient, EventEmitter);

CSGOClient.prototype.setModels = function(models) {
	this.models = models;
};

CSGOClient.prototype.getRanks = function(accountIds, showRanks, callback) {
	if (!this.ready) {
		return callback({success: false, error: "GC client is not ready"});
	}
	if (!accountIds.length) {
		return callback({success: false, error: "Empty query"})
	}
	response = {};
	if (accountIds.length > 20) {
		accountIds = accountIds.slice(0, 21);
		response.warning = "Maximum 20 profiles at once are allowed. Your request has been truncated.";
	}
	var requestId = Math.floor((Math.random() * 10000) + 1),
		eventName = 'players-' + requestId,
		message = cstrike15_gcmessages.CMsgGCCStrike15_v2_ClientRequestPlayersProfile.serialize({
			requestId: requestId,
			accountIds: accountIds,
		}),
		self = this;
	this.steam.toGC(this._appid, CSGO.EMsg.ClientRequestPlayersProfile | protoMask, message);
	var responseHandler = function(playersRes) {
		clearTimeout(responseTimeout);
		response.success = true;
		response.showRanks = showRanks;
		var steamIdToPlayerIdx = {};
		response.accountProfiles = [];
		if ('accountProfiles' in playersRes) {
			response.accountProfiles = playersRes.accountProfiles.map(function(profile, idx) {
				steamIdToPlayerIdx[profile.steamId = self.toSteamID(profile.accountId)] = idx;
				if (profile.ranking) {
					if (showRanks) {
						profile.ranking.rankName = CSGO.MMRanks[profile.ranking.rankId];
						profile.ranking.rankNameAbbr = CSGO.MMRanksAbbr[profile.ranking.rankId];
					} else {
						delete profile.ranking.rankId;
					}
				}
				if (profile.penaltySeconds) {
					profile.penaltyName = CSGO.PenaltyReasons[profile.penaltyReason] || "Unknown (" + profile.penaltyReason +  ")";
					profile.penaltyTime = moment().add(profile.penaltySeconds, 'seconds').fromNow();
				};
				profile.cheater = profile.penaltyReason == 10 || profile.vacBanned;
				return profile;
			});
		}
		var steamIds = accountIds.map(self.toSteamID);
		var steamReceivedIds = Object.keys(steamIdToPlayerIdx);
		message = Steam.Internal.CMsgClientRequestFriendData.serialize({
			personaStateRequested: 3154,
			friends: steamIds
		})
		var friendHandler = function(friend) {
			var steamReceivedIdsIdx = steamReceivedIds.indexOf(friend.friendid),
				accountId = self.toAccountID(friend.friendid);
			if (steamReceivedIdsIdx == -1) {
				if (accountIds.indexOf(accountId) == -1) {
					// we don't care about that friend right now
					return;
				}
				var accountProfilesId = response.accountProfiles.push({
					accountId: accountId,
					steamId: friend.friendid
				}) - 1;
			} else {
				var accountProfilesId = steamIdToPlayerIdx[friend.friendid];
			}
			var avatarHash = friend.avatarHash.toString('hex');
			if (avatarHash != 0)
				response.accountProfiles[accountProfilesId].avatarUrl = [
					"http://media.steampowered.com/steamcommunity/public/images/avatars/",
					avatarHash.substring(0, 2), "/", avatarHash, "_medium.jpg"].join("");
			response.accountProfiles[accountProfilesId].playerName = friend.playerName;
			steamIdsIdx = steamIds.indexOf(friend.friendid);
			if (steamIdsIdx != -1)
				steamIds.splice(steamIdsIdx, 1);
			if (!steamIds.length) {
				self.steam.removeListener('user', friendHandler);
				response.accountProfiles.map(function(profile) {
					winston.info(profile.steamId + " | " + (profile.ranking ? profile.ranking.rankName : "-"));
				});
				callback(response);
			}
		};
		self.steam.on('user', friendHandler);
		self.steam._send(Steam.EMsg.ClientRequestFriendData | protoMask, message);
	};
	this.once(eventName, responseHandler);
	var responseTimeout = setTimeout(function() {
		self.removeListener(eventName, responseHandler);
		callback({success: false, error: "ClientRequestPlayersProfile method timed out"});
	}, 4000);
};

CSGOClient.prototype.toAccountIDAsync = function(steamid, callback) {
	VANITY_BASE = "steamcommunity.com/id/";
	self = this;
	if (steamid.indexOf(VANITY_BASE) > -1) { // Vanity URL
		result = steamid.split("/");
		steamid = result[result.indexOf("id") + 1];
		this.webapi.resolveVanityURL({vanityurl: steamid}, function(err, data) {
			if (err) return winston.log('warn', err);
			callback(null, self.toAccountID(data.steamid));
		});
	} else {
		callback(null, this.toAccountID(steamid));
	}
};

CSGOClient.prototype.toAccountID = function(steamid) {
	PROFILE_BASE = "steamcommunity.com/profiles/";
	if (!steamid) {
		return 0;
	}
	if (steamid[0] == "S" || steamid[0] == 's') { // SteamID32
		result = steamid.split(":");
		if (result.length < 3)
			return 0;
		return +result[1] + result[2] * 2;
	} else if (steamid[0] == "[") { // CommunityID32
		result = steamid.slice(1, -1).split(":");
		if (result.length < 3)
			return 0;
		return (result[2] - 1) / 2;
	} else if (steamid.indexOf(PROFILE_BASE) > -1) { // Profile URL
		result = steamid.split("/");
		steamid = result[result.indexOf("profiles") + 1];
	} // SteamID64
	return bignum(steamid).and(0xFFFFFFFF).toNumber();	
};

CSGOClient.prototype.toSteamID = function(accountid) {
	if (accountid[0] == "S" || accountid[0] == 's') { // yes
		return this.toSteamID(this.toAccountID(accountid));
	}
	return bignum(0x1100001).shiftLeft(32).or(accountid).toString();
};

var handlers = CSGOClient.prototype._handlers = {};

handlers[CSGO.EMsg.ClientWelcome] = function clientWelcomeHandler(message) {
	if (this._gcClientHelloIntervalId) {
		clearInterval(this._gcClientHelloIntervalId);
		this._gcClientHelloIntervalId = null;
		this.ready = true;
		winston.info("GC ready");
	}
};

handlers[CSGO.EMsg.ClientConnectionStatus] = function clientConnectionStatusHandler(message) {
	var status = gcsdk_gcmessages.CMsgConnectionStatus.parse(message).status;

	switch (status) {
		case CSGO.GCConnectionStatus.GCConnectionStatus_HAVE_SESSION:
		winston.info("GC Connection Status regained.");
		if (this._gcClientHelloIntervalId) {
			clearInterval(this._gcClientHelloIntervalId);
			this._gcClientHelloIntervalId = null;
			this.ready = true;
			winston.info("GC ready");
		}
		break;
    default:
		winston.info("GC Connection Status unreliable - " + status);
		if (!this._gcClientHelloIntervalId) {
			this._gcClientHelloIntervalId = setInterval(this._sendHello, 2500);
			this.ready = false;
			winston.info("GC not ready");
		}
		break;
	}
};

handlers[CSGO.EMsg.PlayersProfile] = function playersProfileHandler(message) {
	contents = cstrike15_gcmessages.CMsgGCCStrike15_v2_PlayersProfile.parse(message);
	this.emit('players-' + contents.requestId, contents);
};

CSGO.CSGOClient = CSGOClient;
