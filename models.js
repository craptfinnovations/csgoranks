var Sequelize = require('sequelize'),
	sequelize = new Sequelize('csgoranks.sqlite', '', '', {
	dialect: "sqlite",
});

var Cooldown = new sequelize.define('Cooldown', {
	reason: Sequelize.INTEGER,
	endsAt: Sequelize.DATE,
});
var User = new sequelize.define('User', {
	accountId: Sequelize.INTEGER,
	hits: Sequelize.INTEGER,
});

(function(m) {
	m.User.hasMany(m.Cooldown);
})(module.exports);

module.exports.Cooldown = Cooldown;
module.exports.User = User;
module.exports.sequelize = sequelize;